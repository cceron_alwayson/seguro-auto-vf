import React, { Component } from 'react';
import '@scotia/canvas-dom/css/index.css';
import infoShield from './assets/info-shield.svg';
import {
  InputSelect,
  TabAction,
  TabContent,
  InputCheckbox,
  Direction,
  Grid,
  Margin,
  SubHeading,
  Heading,
  Ruler,
  Column,
  InputText,
  ButtonBack,
  ButtonComplete,
  TextCaption,
  BuddyTip,
  BuddyTipFlat,
  BuddyTipText,
  BuddyTipIcon,
  Text,
} from '@scotia/canvas-react';
import Terminos from './Modales/terminos-y-condiciones';
import Mandato from './Modales/mandato';

class App extends Component {
  componentWillMount() {
    this.setState({ 'tabs': 1 });
  }

  Tab = (tabValue) => {
    this.setState({ 'tabs': tabValue });
  }

  render() {
    const tab = this.state.tabs;
    let tab1 = <TabAction onClick={() => this.Tab(1)} className="tab-adj2">PAC</TabAction>;
    let tab2 = <TabAction onClick={() => this.Tab(2)} className="tab-adj2">PAT</TabAction>;
    let tabContent;

    if (tab === 1) {
      tab1 = <TabAction isActive onClick={() => this.Tab(1)} className="tab-adj2">PAC</TabAction>
      tabContent = <TabContent isActive>

        <Margin lg={36} xs={36}>
          <Direction>
            <Column lgOffset={2}
              lg={4}
              xs={12}>
                <Margin xs={24} side="bottom">
              <BuddyTip className="no-flex no-shadow">
                <Margin lg={24} xs={24} side="bottom">
                  <SubHeading className="pl18">
                    Resumen
            </SubHeading>
                </Margin>

                <Margin lg={12} xs={12} side="bottom">
                  <Text className="pl18">
                    Datos del vehículo
            </Text>
                </Margin>

                <Direction>
                  <Column lg={6} xs={6}>
                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Marca
              </TextCaption>
                    </Margin>
                    <Margin lg={24} xs={24} side="bottom">
                      <SubHeading>
                        Toyota
              </SubHeading>
                    </Margin>

                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Año
              </TextCaption>
                    </Margin>
                    <Margin lg={12} xs={12} side="bottom">
                      <SubHeading>
                        2015
              </SubHeading>
                    </Margin>
                  </Column>

                  <Column lg={6} xs={6}>

                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Patente
              </TextCaption>
                    </Margin>
                    <Margin lg={24} xs={24} side="bottom">
                      <SubHeading>
                        BZ AA 55
              </SubHeading>
                    </Margin>

                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Color
              </TextCaption>
                    </Margin>
                    <Margin lg={12} xs={12} side="bottom">
                      <SubHeading>
                        Amarillo
              </SubHeading>
                    </Margin>
                  </Column>
                </Direction>

                <Column lg={12} className="hide--xs">
                  <Ruler />
                </Column>

                <Direction>
                  <Column lg={6} xs={6}>
                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Plan
              </TextCaption>
                    </Margin>
                    <Margin lg={24} xs={24} side="bottom">
                      <SubHeading>
                        Anual
              </SubHeading>
                    </Margin>

                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Valor mensual
              </TextCaption>
                    </Margin>
                    <Margin lg={12} xs={12} side="bottom">
                      <SubHeading>
                        $ 24.000 (UF 4)
              </SubHeading>
                    </Margin>
                  </Column>

                  <Column lg={6} xs={6}>
                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Aseguradora
              </TextCaption>
                    </Margin>
                    <Margin lg={24} xs={24} side="bottom">
                      <SubHeading>
                        Zenit
              </SubHeading>
                    </Margin>

                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Deducible
              </TextCaption>
                    </Margin>
                    <Margin lg={12} xs={12} side="bottom">
                      <SubHeading>
                        UF 5
              </SubHeading>
                    </Margin>
                  </Column>
                </Direction>

              </BuddyTip>
              </Margin>
            </Column>

            <Column lgOffset={1}
              lg={3} xs={12}>
              <Margin lg={24} xs={24} side="bottom">
                <InputSelect
                  id="select-1"
                  name="select-1"
                  label="Tu Cuenta Corriente"
                  defaultValue=""
                  className="test">

                  <option value="" disabled hidden>
                    ****1234
              </option>
                  <option value="option1">Option 1</option>
                  <option value="option2">Option 2</option>
                  <option value="option3">Option 3</option>
                </InputSelect>
              </Margin>
              <Margin lg={24} xs={24}>
                <div className="flex-end-bottom">
                  <InputCheckbox
                    id="terminos y condiciones pac"
                    name="terminos y condiciones pac"
                    label="Acepto los" />
                  <div className="r-4">
                    <Terminos />
                  </div>
                </div>

              </Margin>
              <div className="flex-end-bottom">
                <InputCheckbox
                  id="mandato pac"
                  name="mandato pac"
                  label="Acepto" />
                <div className="r-4">
                  <Mandato />
                </div>
              </div>
            </Column>
          </Direction>
        </Margin>

        <Column lgOffset={2} lg={8} xs={12}>
          <Margin lg={36} xs={24} side="bottom">
            <BuddyTipFlat>
              <BuddyTipIcon>
                <img src={infoShield} alt="icon" />
              </BuddyTipIcon>
              <BuddyTipText>
                Este medio de pago es el que utilizarás para el pago de tu seguro. La fecha de cobro será la próxima a la definida por la compañia aseguradora, una vez que comience a regir la cobertura del seguro.
            </BuddyTipText>
            </BuddyTipFlat>
          </Margin>
        </Column>

        <Column lgOffset={2} lg={8} xs={12}>
          <Margin lg={36} xs={36} className='space-between'>
            <ButtonBack className="r18">Volver al Home</ButtonBack>
            {/* boton con cambio de estado relacionado al checkbox*/}
            <ButtonComplete className="text-red">Aceptar</ButtonComplete>
          </Margin>
        </Column>

      </TabContent>
    }

    if (tab === 2) {
      tab2 = <TabAction isActive onClick={() => this.Tab(2)} className="tab-adj2">PAT</TabAction>
      tabContent = <TabContent isActive>

        <Margin lg={36} xs={36}>
          <Direction>

            <Column lgOffset={2}
              lg={4}
              xs={12}>
                <Margin xs={24} side="bottom">
              <BuddyTip className="no-flex no-shadow">
                <Margin lg={24} xs={24} side="bottom">
                  <SubHeading className="pl18">
                    Resumen
            </SubHeading>
                </Margin>

                <Margin lg={12} xs={12} side="bottom">
                  <Text className="pl18">
                    Datos del vehículo
            </Text>
                </Margin>

                <Direction>
                  <Column lg={6} xs={6}>
                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Marca
              </TextCaption>
                    </Margin>
                    <Margin lg={24} xs={24} side="bottom">
                      <SubHeading>
                        Toyota
              </SubHeading>
                    </Margin>

                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Año
              </TextCaption>
                    </Margin>
                    <Margin lg={12} xs={12} side="bottom">
                      <SubHeading>
                        2015
              </SubHeading>
                    </Margin>
                  </Column>

                  <Column lg={6} xs={6}>

                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Patente
              </TextCaption>
                    </Margin>
                    <Margin lg={24} xs={24} side="bottom">
                      <SubHeading>
                        BZ AA 55
              </SubHeading>
                    </Margin>

                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Color
              </TextCaption>
                    </Margin>
                    <Margin lg={12} xs={12} side="bottom">
                      <SubHeading>
                        Amarillo
              </SubHeading>
                    </Margin>
                  </Column>
                </Direction>

                <Column lg={12} className="hide--xs">
                  <Ruler />
                </Column>

                <Direction>
                  <Column lg={6} xs={6}>
                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Plan
              </TextCaption>
                    </Margin>
                    <Margin lg={24} xs={24} side="bottom">
                      <SubHeading>
                        Anual
              </SubHeading>
                    </Margin>

                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Valor mensual
              </TextCaption>
                    </Margin>
                    <Margin lg={12} xs={12} side="bottom">
                      <SubHeading>
                        $ 24.000 (UF 4)
              </SubHeading>
                    </Margin>
                  </Column>

                  <Column lg={6} xs={6}>
                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Aseguradora
              </TextCaption>
                    </Margin>
                    <Margin lg={24} xs={24} side="bottom">
                      <SubHeading>
                        Zenit
              </SubHeading>
                    </Margin>

                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Deducible
              </TextCaption>
                    </Margin>
                    <Margin lg={12} xs={12} side="bottom">
                      <SubHeading>
                        UF 5
              </SubHeading>
                    </Margin>
                  </Column>
                </Direction>

                <div className="flex-end-right">
                  <TextCaption>Se adjuntó Factura Nro. 219031010</TextCaption>
                </div>

              </BuddyTip>
              </Margin>
            </Column>

            <Column lgOffset={1}
              lg={3} xs={12}>
              <Margin lg={24} xs={24} side="bottom">
                <InputSelect
                  id="select-1"
                  name="select-1"
                  label="Número de tarjeta"
                  defaultValue=""
                  className="test">

                  <option value="" disabled hidden>
                    ****1234
              </option>
                  <option value="option1">Option 1</option>
                  <option value="option2">Option 2</option>
                  {/* si la opcion es "otros" se debe aplicar la clase "display-block" sobre "otra tarjeta" */}
                  <option value="option3">Otra</option>
                </InputSelect>
              </Margin>

              {/* otra tarjeta */}
              <div className="display-non">
              <Margin lg={24} xs={24} side="bottom">
                <InputSelect
                  id="select-1"
                  name="select-1"
                  label="Nombre del banco"
                  defaultValue=""
                  className="test">

                  <option value="" disabled hidden>
                    EJ: Banco de la ciudad
              </option>
                  <option value="option1">Option 1</option>
                  <option value="option2">Option 2</option>
                  <option value="option3">Option 3</option>
                </InputSelect>
              </Margin>

              <Margin lg={24} xs={24} side="bottom">
              <InputText
                id="input-1"
                type="text"
                label="Escríbe el número"
                placeholder="Ej: xxx789"
                errorMsg="Field specific error message" />
              </Margin>
              </div>

              <Margin lg={24} xs={24} side="bottom">
              <InputText
                id="input-1"
                type="text"
                label="Fecha de vencimiento"
                placeholder="MM/YY"
                errorMsg="Field specific error message" />
              </Margin>
              
              <Margin lg={24} xs={24}>
                <div className="flex-end-bottom">
                  <InputCheckbox
                    id="terminos y condiciones pat"
                    name="terminos y condiciones pat"
                    label="Acepto los" />
                  <div className="r-4">
                    <Terminos />
                  </div>
                </div>

              </Margin>
              <div className="flex-end-bottom">
                <InputCheckbox
                  id="mandato pat"
                  name="mandato pat"
                  label="Acepto" />
                <div className="r-4">
                  <Mandato />
                </div>
              </div>
            </Column>
          </Direction>
        </Margin>

        <Column lgOffset={2} lg={8} xs={12}>
          <Margin lg={36} xs={24} side="bottom">
            <BuddyTipFlat>
              <BuddyTipIcon>
                <img src={infoShield} alt="icon" />
              </BuddyTipIcon>
              <BuddyTipText>
                Este medio de pago es el que utilizarás para el pago de tu seguro. La fecha de cobro será la próxima a la definida por la compañia aseguradora, una vez que comience a regir la cobertura del seguro.
            </BuddyTipText>
            </BuddyTipFlat>
          </Margin>
        </Column>

        <Column lgOffset={2} lg={8} xs={12}>
          <Margin lg={36} xs={36} className='space-between'>
            <ButtonBack>Volver a simular</ButtonBack>
            {/* boton con cambio de estado relacionado al checkbox*/}
            <ButtonComplete className="text-red">Aceptar</ButtonComplete>
          </Margin>
        </Column>

      </TabContent>
    }

    return (

      <Grid>

        <Column lgOffset={2}
          lg={8}
          xs={12}>
          <Margin
            lg={36}
            xs={36}
            side="top">
            <div className="hide--xs">
              <SubHeading>
                Seguro Automotriz
                </SubHeading>
            </div>
            <Heading>
              Elige tu medio de pago
              </Heading>
          </Margin>
        </Column>

        <Margin lg={24}
          xs={24}>
          <Direction>
            <Column lgOffset={2}
              lg={2} xs={6}>
              {tab1}
            </Column>
            <Column
              lg={2}
              xs={6}>
              {tab2}
            </Column>
          </Direction>


          <Column lgOffset={2}
            lg={8} xs={12}>
            <Ruler className="m0" />
          </Column>
        </Margin>
        {tabContent}

      </Grid>





    );

  }
}

export default App;