import React from "react";
import PropTypes from "prop-types";
import { compose, withHandlers } from "recompose";
import '@scotia/canvas-dom/css/index.css';
import {
  Modal,
  DialogHeader,
  ButtonPill,
  withToggle,
  Grid,
  Heading,
  TableBody,
  TableData,
  TableHeader,
  TableRow,
  TableHeaderData, Badge,
  Margin,
  TextCaption,
  Direction,
  Text,
  Column,
  Table,
  IconPdf,
  BrandLine,
  Ruler
} from "@scotia/canvas-react";

const renderCard = onClick => (
  <ButtonPill className='ver-tabla-comparativa' onClick={onClick}>Ver tabla comparativa</ButtonPill>
);

const renderModal = (toggled, onClick, onClose) => (
  <Modal isOpen={toggled} onClose={onClose} showButtonClose>

    <DialogHeader>
      <Heading>Tabla comparativa de Planes</Heading>
    </DialogHeader>
    <Margin lg={12} xs={12} side="top">
    <BrandLine/>
    </Margin>

    <Margin 
          lg={36}
          xs={36}
          side="bottom">
        <Table>
          <TableHeader>
            <TableRow>
              <TableHeaderData>
              </TableHeaderData>

              <TableHeaderData>
                <Column>
                <Margin 
                lg={12}
                xs={12}>
                  <div className='derecha'>
                  <Badge variant="success" className='badge-card derecha'>
                    Más comprado
                  </Badge>
                  </div>
                  </Margin>
                  <div className='b-right'>
                    <TextCaption>
                      Zenit
                    </TextCaption>
                  <TextCaption>
                    Valor mensual
                    </TextCaption>
                  <Direction>
                    <Text>
                      $24.500
                      </Text>
                    <TextCaption className='card-footer-text light'>
                      (UF 5)
                      </TextCaption>
                  </Direction>
                  <TextCaption className='card-footer-text'>
                    deducible UF 10
                    </TextCaption>
                    </div>
                </Column>
              </TableHeaderData>

              <TableHeaderData>
                <Column>
                <Margin 
                lg={12}
                xs={12}>
                  <div className='derecha'>
                  <Badge variant="success" className='badge-card derecha'>
                    Más comprado
                  </Badge>
                  </div>
                  </Margin>
                  <div className='b-right'>
                    <TextCaption>
                      BNP PARIBAS
                    </TextCaption>
                  <TextCaption>
                    Valor mensual
                    </TextCaption>
                  <Direction>
                    <Text>
                      $25.500
                      </Text>
                    <TextCaption className='card-footer-text light'>
                      (UF 6)
                      </TextCaption>
                  </Direction>
                  <TextCaption className='card-footer-text'>
                    deducible UF 5
                    </TextCaption>
                    </div>
                </Column>
              </TableHeaderData>

              <TableHeaderData>
                <Column>
                <Margin 
                lg={12}
                xs={12}>
                <div className='derecha'>
                  <Badge variant="success" className='badge-card derecha'>
                    Más comprado
                  </Badge>
                  </div>
                  </Margin>
                    <TextCaption>
                      LIBERTY Seguros
                    </TextCaption>
                  <TextCaption>
                    Valor mensual
                    </TextCaption>
                  <Direction>
                    <Text>
                      $24.000
                      </Text>
                    <TextCaption className='card-footer-text light'>
                      (UF 2)
                      </TextCaption>
                  </Direction>
                  <TextCaption className='card-footer-text'>
                    deducible UF 10
                    </TextCaption>
                </Column>
              </TableHeaderData>
            </TableRow>
          </TableHeader>

          <TableBody>
            <TableRow>
              <TableData>
              Autos de reemplazo por <br></br>siniestro
              </TableData>
              <TableData>
              h/15 días copago $ 4.000
              </TableData>
              <TableData>
              h/45 días copago $ 4.000
              </TableData>
              <TableData>
              h/45 días copago $ 4.000
              </TableData>
            </TableRow>

            <TableRow>
              <TableData>
              Servicio de revisión técnica
            </TableData>
              <TableData>
              No incluida
            </TableData>
              <TableData>
              Incluida Evento de un año
            </TableData>
            <TableData>
            Incluida Evento de un año
            </TableData>
            </TableRow>

            <TableRow>
              <TableData>
              Mantención por km
            </TableData>
              <TableData>
              No incluida
            </TableData>
              <TableData>
              Incluida Evento de un año
            </TableData>
            <TableData>
            Incluida Evento de un año
            </TableData>
            </TableRow>
            
            <TableRow>
              <TableData>
              Taller de reparación
            </TableData>
              <TableData>
              Taller de convenio
            </TableData>
              <TableData>
              Taller oficial de la marca
            </TableData>
            <TableData>
              Taller oficial de la marca
            </TableData>
            </TableRow>

            <TableRow>
              <TableData>
              Responsabilidad civil
            </TableData>
              <TableData>
              H/ UF 500
            </TableData>
              <TableData>
              H/ UF 1.000
            </TableData>
            <TableData>
              H/ UF 1.000
            </TableData>
            </TableRow>

          </TableBody>
          </Table>
          <Margin lg={12} xs={12} side="bottom">
          <Ruler/>
          </Margin>

        <Direction>
          <Column lg={3}>
          <TextCaption className="footer-text light">
                Ver detalles
              </TextCaption>
          </Column>
          <Column lg={3}>
          <div className='circulo-icon'>
              <IconPdf className='pdf-icon'/>
              </div>
          </Column>
          <Column lg={3}>
          <div className='circulo-icon'>
              <IconPdf className='pdf-icon'/>
              </div>
          </Column>
          <Column lg={3}>
          <div className='circulo-icon'>
              <IconPdf className='pdf-icon'/>
              </div>
          </Column>
          </Direction>
          </Margin>
        


  </Modal>
);

export const TablaComparativa = ({ toggled, onClick, onClose }) => (
  <Grid className='centrar'>
    {renderCard(onClick)}
    {renderModal(toggled, onClick, onClose)}

  </Grid>
);

TablaComparativa.propTypes = {
  toggled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export const ModalExample = compose(
  withToggle,
  withHandlers({
    onClick: ({ toggle }) => event => {
      event.preventDefault();
      toggle();
    },
    onClose: ({ toggle }) => () => toggle()
  })
)(TablaComparativa);

export default ModalExample;