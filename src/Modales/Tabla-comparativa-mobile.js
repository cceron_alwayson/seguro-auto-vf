import React from "react";
import PropTypes from "prop-types";
import { compose, withHandlers } from "recompose";
import '@scotia/canvas-dom/css/index.css';
import {
    Modal,
    DialogHeader,
    ButtonPill,
    withToggle,
    Grid,
    SubHeading,
    TableBody,
    TableData,
    TableHeader,
    TableRow,
    TableHeaderData, Badge,
    Margin,
    TextCaption,
    Direction,
    Text,
    Column,
    Table,
    IconPdf,
    BrandLine,
    Ruler
} from "@scotia/canvas-react";

const renderCard = onClick => (
    <ButtonPill className='ver-tabla-comparativa' onClick={onClick}>Ver tabla comparativa</ButtonPill>
);

const renderModal = (toggled, onClick, onClose) => (
    <Modal isOpen={toggled} onClose={onClose} showButtonClose>

        <DialogHeader>
            <Margin xs={36} side="top">
                <SubHeading>Tabla comparativa de Planes</SubHeading>
            </Margin>
        </DialogHeader>
        <Margin xs={12}>
            <BrandLine />
        </Margin>

        <Margin
            lg={36}
            xs={36}
            side="bottom">

            <Direction>

                <Column xs={6} className="pl0">
                    <Margin
                        xs={12}>
                        <Badge variant="success" className='badge-card derecha'>
                            Más comprado
                  </Badge>
                    </Margin>
                    <TextCaption>
                        Zenit
                    </TextCaption>
                </Column>


                <Column xs={6}>
                    <Margin xs={60} side="top">
                        <Direction className="f-derecha">
                            <Text>
                                $24.500
                      </Text>
                            <TextCaption className='card-footer-text light'>
                                (UF 5)
                      </TextCaption>
                        </Direction>
                        <TextCaption className='card-footer-text flex f-derecha'>
                            deducible UF 10
                    </TextCaption>
                    </Margin>
                </Column>
            </Direction>


            <Table>
                <TableBody>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Autos de reemplazo por siniestro
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    h/15 días copago $ 4.000
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Servicio de revisión técnica
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    No incluida
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Mantención por km
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    No incluida
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Taller de reparación
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    Taller de convenio
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Responsabilidad civil
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    H/ UF 500
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                </TableBody>
            </Table>

            <Direction>

                <Column xs={6}>
                    <TextCaption className="footer-text light">
                        Ver detalles
              </TextCaption>
                </Column>

                <Column xs={6} className="flex f-derecha">
                    <IconPdf />
                </Column>

            </Direction>

            <Margin xs={12}>
                <Ruler />
            </Margin>

            <Direction>

                <Column xs={6} className="pl0">
                    <Margin
                        xs={12}>
                        <Badge variant="success" className='badge-card derecha'>
                            Más comprado
                  </Badge>
                    </Margin>
                    <TextCaption>
                        Zenit
                    </TextCaption>
                </Column>


                <Column xs={6}>
                    <Margin xs={60} side="top">
                        <Direction className="f-derecha">
                            <Text>
                                $24.500
                            </Text>
                            <TextCaption className='card-footer-text light'>
                                (UF 5)
                      </TextCaption>
                        </Direction>
                        <TextCaption className='card-footer-text flex f-derecha'>
                            deducible UF 10
                    </TextCaption>
                    </Margin>
                </Column>
            </Direction>


            <Table>
                <TableBody>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Autos de reemplazo por siniestro
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    h/15 días copago $ 4.000
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Servicio de revisión técnica
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    No incluida
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Mantención por km
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    No incluida
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Taller de reparación
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    Taller de convenio
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Responsabilidad civil
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    H/ UF 500
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                </TableBody>
            </Table>

            <Direction>

                <Column xs={6}>
                    <TextCaption className="footer-text light">
                        Ver detalles
              </TextCaption>
                </Column>

                <Column xs={6} className="flex f-derecha">
                    <IconPdf />
                </Column>

            </Direction>

            <Margin xs={12}>
                <Ruler />
            </Margin>

            <Direction>

                <Column xs={6} className="pl0">
                    <Margin
                        xs={12}>
                        <Badge variant="success" className='badge-card derecha'>
                            Más comprado
                  </Badge>
                    </Margin>
                    <TextCaption>
                        Zenit
                    </TextCaption>
                </Column>


                <Column xs={6}>
                    <Margin xs={60} side="top">
                        <Direction className="f-derecha">
                            <Text>
                                $24.500
                      </Text>
                            <TextCaption className='card-footer-text light'>
                                (UF 5)
                      </TextCaption>
                        </Direction>
                        <TextCaption className='card-footer-text flex f-derecha'>
                            deducible UF 10
                    </TextCaption>
                    </Margin>
                </Column>
            </Direction>


            <Table>
                <TableBody>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Autos de reemplazo por siniestro
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    h/15 días copago $ 4.000
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Servicio de revisión técnica
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    No incluida
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Mantención por km
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    No incluida
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Taller de reparación
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    Taller de convenio
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                    <TableRow>
                        <Direction>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pb0">
                                    Responsabilidad civil
                                </TableData>
                            </Column>
                            <Column lg={6} xs={12}>
                                <TableData className="xs-pt0">
                                    H/ UF 500
                                </TableData>
                            </Column>
                        </Direction>
                    </TableRow>

                </TableBody>
            </Table>

            <Direction>

                <Column xs={6}>
                    <TextCaption className="footer-text light">
                        Ver detalles
              </TextCaption>
                </Column>

                <Column xs={6} className="flex f-derecha">
                    <IconPdf />
                </Column>

            </Direction>

            <Margin xs={12}>
                <Ruler />
            </Margin>


        </Margin>

        <Margin
            lg={36}
            xs={36}
            side="bottom" className="flex f-derecha">
            <ButtonPill onClick={onClose}>
                Entendido
    </ButtonPill>

        </Margin>



    </Modal>
);

export const TablaComparativa = ({ toggled, onClick, onClose }) => (
    <Grid className='centrar'>
        {renderCard(onClick)}
        {renderModal(toggled, onClick, onClose)}

    </Grid>
);

TablaComparativa.propTypes = {
    toggled: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired
};

export const ModalExample = compose(
    withToggle,
    withHandlers({
        onClick: ({ toggle }) => event => {
            event.preventDefault();
            toggle();
        },
        onClose: ({ toggle }) => () => toggle()
    })
)(TablaComparativa);

export default ModalExample;