import React from "react";
import PropTypes from "prop-types";
import { compose, withHandlers } from "recompose";
import '@scotia/canvas-dom/css/index.css';
import zenit from '../assets/zenit-seguros-logo.png';
import EnviarSimulacion from './enviar-simulacion';
import {
  Modal,
  DialogHeader,
  withToggle,
  Heading,
  TableBody,
  TableData,
  TableRow,
  TableHeaderData,
  Margin,
  TextCaption,
  Column,
  Table,
  Link,
  IconAddOutline,
  Direction,
  ButtonPill,
  InputCheckbox,
  InputText
} from "@scotia/canvas-react";
import { BrandLine, Ruler } from "@scotia/canvas-react/lib/core";

const renderCard = onClick => (
  <Link className='ver-detalles' onClick={onClick}>Enviar detalles <IconAddOutline size={12} /></Link>
);

const renderModal = (toggled, onClick, onClose) => (

  <Modal isOpen={toggled} onClose={onClose} showButtonClose className="modal-ver">

    <Column lg={6} xs={12} className="p0">
      <Margin lg={24} xs={24}>
      <Direction>
            <Column lg={3} xs={3} className="pl0">
              <img src={zenit} alt="logo" className='card-logo' />
            </Column>

            <Column lg={4} xs={4} className='b-left v-vertical-centrar f-centrar'>
              <TextCaption>
                Plan Básico
                </TextCaption>
            </Column>
            <Column lg={5} xs={5} className='b-left v-vertical-centrar f-centrar'>
              <TextCaption>
                Vigencia Anual
                </TextCaption>
            </Column>
            </Direction>
            </Margin>

    </Column>

    <DialogHeader className="mt5">
      <Heading>Detalles del plan</Heading>
    </DialogHeader>

    <Margin>
      <TextCaption className="mt5">
        La simulacion tiene una vigencia de 30 días a contar del momento en que la solicitas.
      </TextCaption>
      <BrandLine className="mt5 hide--xs" />
    </Margin>

    <Margin
      lg={24}
      xs={24}
      side="bottom">

      <Table>
        <TableBody>

          <TableRow>
            <Direction>
            <Column lg={6} xs={12}>
            <TableData className="xs-pb0">
              Daños materiales
              </TableData>
              </Column>
              <Column lg={6} xs={12}>
            <TableData className="xs-pt0">
              Valor comercial
              </TableData>
              </Column>
              </Direction>
          </TableRow>

          <TableRow>
            <Direction>
            <Column lg={6} xs={12}>
            <TableData className="xs-pb0">
            Robos, hurto, uso no autorizado
              </TableData>
              </Column>
              <Column lg={6} xs={12}>
            <TableData className="xs-pt0">
            Valor comercial
              </TableData>
              </Column>
              </Direction>
          </TableRow>
         
          <TableRow>
            <Direction>
            <Column lg={6} xs={12}>
            <TableData className="xs-pb0">
            Responsabilidad civil daño emergente
              </TableData>
              </Column>
              <Column lg={6} xs={12}>
            <TableData className="xs-pt0">
            UF 1.000
              </TableData>
              </Column>
              </Direction>
          </TableRow>

          <TableRow>
            <Direction>
            <Column lg={6} xs={12}>
            <TableData className="xs-pb0">
            Responsabilidad civil lucro cesante
              </TableData>
              </Column>
              <Column lg={6} xs={12}>
            <TableData className="xs-pt0">
            UF 1.000
              </TableData>
              </Column>
              </Direction>
          </TableRow>

          <TableRow>
            <Direction>
            <Column lg={6} xs={12}>
            <TableData className="xs-pb0">
            Responsabilidad civil daño moral
              </TableData>
              </Column>
              <Column lg={6} xs={12}>
            <TableData className="xs-pt0">
            UF 1.000
              </TableData>
              </Column>
              </Direction>
          </TableRow>

          <TableRow>
            <Direction>
            <Column lg={6} xs={12}>
            <TableData className="xs-pb0">
            Robo de accesorios
              </TableData>
              </Column>
              <Column lg={6} xs={12}>
            <TableData className="xs-pt0">
            10% Valor comercial tope UF 50
              </TableData>
              </Column>
              </Direction>
          </TableRow>

        </TableBody>
      </Table>
      <Ruler />
    </Margin>

    <Direction>
      <Column lg={6} xs={12}>
        <Margin xs={18} side="bottom">
        <InputCheckbox
          id="dec-uso-part"
          name="dec-uso-part"
          label="Enviar el plan a su e-mail" />
          </Margin>
      </Column>

      <Column lg={6} xs={12}>
        <InputText
          id="input-1"
          type="text"
          label=""
          placeholder="a****@gmail.com"
          errorMsg="Field specific error message" />
      </Column>
    </Direction>

    <Margin
      lg={12}
      xs={12} className='flex-end-right'>
        <ButtonPill onClick={onClose} className='mtb-20 bla'>
          Cancelar
    </ButtonPill>
        <EnviarSimulacion/>
    </Margin>

  </Modal>
);

export const EnviarDetalles = ({ toggled, onClick, onClose }) => (
  <Column>
    {renderCard(onClick)}
    {renderModal(toggled, onClick, onClose)}

  </Column>
);

EnviarDetalles.propTypes = {
  toggled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export const ModalExample = compose(
  withToggle,
  withHandlers({
    onClick: ({ toggle }) => event => {
      event.preventDefault();
      toggle();
    },
    onClose: ({ toggle }) => () => toggle()
  })
)(EnviarDetalles);

export default ModalExample;