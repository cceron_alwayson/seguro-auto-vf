import React from "react";
import PropTypes from "prop-types";
import { compose, withHandlers } from "recompose";
import '@scotia/canvas-dom/css/index.css';
import {
  Modal,
  DialogHeader,
  withToggle,
  Heading,
  Text,
  Margin,
  TextCaption,
  Column,
  ButtonPrimary,
  SubHeading,
  Direction,
  ButtonPill,
  IconSuccessIllustrative
} from "@scotia/canvas-react";

const renderCard = onClick => (
    <ButtonPrimary onClick={onClick}>Enviar</ButtonPrimary>
);

const renderModal = (toggled, onClick, onClose) => (

  <Modal isOpen={toggled} onClose={onClose} showButtonClose>
      
      <div className="p36">
        <Margin xs={24} side="bottom" className="xs-success">
      <IconSuccessIllustrative size={72} />
      </Margin>
    <DialogHeader>
        <Margin
            lg={24}
            xs={24}
            side="bottom">
      <Heading>¡Se envió con éxito!</Heading>
      </Margin>
    </DialogHeader>

    <Margin 
          lg={36}
          xs={36} side="bottom">
        <Text>
        Se ha enviado a ver*****@gmail.com para que se actualice la base de datos.
        </Text>
    </Margin>

    <Direction>
    <Column lg={6} xs={12} className="pl0">
            <Margin lg={12} xs={12} side="bottom">
              <TextCaption>
                Marca
              </TextCaption>
              </Margin>
              <Margin lg={24} xs={24} side="bottom">
              <SubHeading>
                Toyota
              </SubHeading>
              </Margin>

              <Margin lg={12} xs={12} side="bottom">
              <TextCaption>
                Año del vehículo
              </TextCaption>
              </Margin>
              <Margin xs={24} side="bottom">
              <SubHeading>
              2018
              </SubHeading>
              </Margin>
            </Column>

            <Column lg={6} xs={12} className="xs-b-left xs-pl0">
            <Margin lg={12} xs={12} side="bottom">
            
              <TextCaption>
                Modelo
              </TextCaption>
              </Margin>
              <Margin xs={24}>
              <SubHeading>
                ASDXX
              </SubHeading>
              </Margin>
            </Column>

            </Direction>
            </div>
            <Column className='f-derecha flex'
        lg={12} xs={12}>
            <Margin
    lg={36}
    xs={36}
    side="bottom">
    <ButtonPill onClick={onClose}>
        Entendido
    </ButtonPill>

    </Margin>
    </Column>
  </Modal>
);

export const CasoOtros = ({ toggled, onClick, onClose }) => (
  <Column>
    {renderCard(onClick)}
    {renderModal(toggled, onClick, onClose)}

  </Column>
);

CasoOtros.propTypes = {
  toggled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export const ModalExample = compose(
  withToggle,
  withHandlers({
    onClick: ({ toggle }) => event => {
      event.preventDefault();
      toggle();
    },
    onClose: ({ toggle }) => () => toggle()
  })
)(CasoOtros);

export default ModalExample;