import React from "react";
import PropTypes from "prop-types";
import { compose, withState } from "recompose";
import '@scotia/canvas-dom/css/index.css';
import {
  Popup,
  PopupHeader,
  PopupContent,
  PopupFooter,
  withToggle,
  ButtonPill,
  ButtonContinue
} from "@scotia/canvas-react";

const renderPopup = (toggled, onClick, popupType) => (
  <Popup isOpen={toggled} type={popupType}>
    <PopupHeader>La fecha excede las 48 horas.</PopupHeader>
    <PopupContent>
    Deberás ponerte en contacto con el 600 XXX XXX.
    </PopupContent>
    <PopupFooter>
      <ButtonPill variant={popupType} onClick={onclose}>
        Entiendo
      </ButtonPill>
    </PopupFooter>
  </Popup>
);

export const WrappedModalExample = ({
  toggled,
  toggle,
  setPopupType,
  popupType
}) => (
        <ButtonContinue variant="help"
          onClick={() => {
            setPopupType("help");
            toggle();
          }}>Continuar
          {toggled && renderPopup(toggled, toggle, popupType)}
          </ButtonContinue>
          
      

);

WrappedModalExample.propTypes = {
  toggled: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  setPopupType: PropTypes.func.isRequired,
  popupType: PropTypes.string.isRequired
};

 const PopupExample = compose(
  withToggle,
  withState("popupType", "setPopupType", "default")
)(WrappedModalExample);

export default PopupExample;
