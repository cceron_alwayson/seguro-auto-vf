import React from "react";
import PropTypes from "prop-types";
import { compose, withHandlers } from "recompose";
import '@scotia/canvas-dom/css/index.css';
import {
  Modal,
  DialogHeader,
  withToggle,
  Column,
  Heading,
  Margin,
  Link,
  TextSmall,
  ButtonPill,
  Direction,
  BrandLine
} from "@scotia/canvas-react";

const renderCard = onClick => (
    
    
  <Link onClick={onClick}>Términos y condiciones</Link>
);

const renderModal = (toggled, onClick, onClose) => (
  <Modal className="p36" isOpen={toggled} onClose={onClose} showButtonClose>

    <Direction>
    <Margin
    lg={12}
    xs={12}
    side="bottom">
    <DialogHeader>
      <Heading>Términos y condiciones</Heading>
    </DialogHeader>
    <Margin lg={6} side="top">
    <BrandLine/>
    </Margin>
    </Margin>
    </Direction>

    <div className="pb36">
    <Direction className="scroll-modal">
    <Margin
    lg={24}
    xs={24}
    side="bottom">
    <TextSmall>
    If you find yourself doing the same routine day after day, 
    never remembering what you did the day before, having nothing to look forward to, 
    living your life – but never enjoying it or anything in it…you are probably living your life and the relationships in it on autopilot. 
    </TextSmall>
    </Margin>

    <Margin
    lg={24}
    xs={24}
    side="bottom">
    <TextSmall>
    If you find yourself doing the same routine day after day, 
    never remembering what you did the day before, having nothing to look forward to, 
    living your life – but never enjoying it or anything in it…you are probably living your life and the relationships in it on autopilot. 
    </TextSmall>
    </Margin>

    <Margin
    lg={24}
    xs={24}
    side="bottom">
    <TextSmall>
    If you find yourself doing the same routine day after day, 
    never remembering what you did the day before, having nothing to look forward to, 
    living your life – but never enjoying it or anything in it…you are probably living your life and the relationships in it on autopilot. 
    </TextSmall>
    </Margin>

    <Margin
    lg={24}
    xs={24}
    side="bottom">
    <TextSmall>
    If you find yourself doing the same routine day after day, 
    never remembering what you did the day before, having nothing to look forward to, 
    living your life – but never enjoying it or anything in it…you are probably living your life and the relationships in it on autopilot. 
    </TextSmall>
    </Margin>

    <Margin
    lg={24}
    xs={24}
    side="bottom">
    <TextSmall>
    If you find yourself doing the same routine day after day, 
    never remembering what you did the day before, having nothing to look forward to, 
    living your life – but never enjoying it or anything in it…you are probably living your life and the relationships in it on autopilot. 
    </TextSmall>
    </Margin>

    <Margin
    lg={24}
    xs={24}
    side="bottom">
    <TextSmall>
    If you find yourself doing the same routine day after day, 
    never remembering what you did the day before, having nothing to look forward to, 
    living your life – but never enjoying it or anything in it…you are probably living your life and the relationships in it on autopilot. 
    </TextSmall>
    </Margin>

    <Margin
    lg={24}
    xs={24}
    side="bottom">
    <TextSmall>
    If you find yourself doing the same routine day after day, 
    never remembering what you did the day before, having nothing to look forward to, 
    living your life – but never enjoying it or anything in it…you are probably living your life and the relationships in it on autopilot. 
    </TextSmall>
    </Margin>

    <Margin
    lg={72}
    xs={72}
    side="bottom">
    <TextSmall>
    If you find yourself doing the same routine day after day, 
    never remembering what you did the day before, having nothing to look forward to, 
    living your life – but never enjoying it or anything in it…you are probably living your life and the relationships in it on autopilot. 
    </TextSmall>
    </Margin>

    <Direction className='f-derecha mr36'>
    <ButtonPill onClick={onClose}>
        Entendido
    </ButtonPill>
    </Direction>
    </Direction>
    </div>
    
  </Modal>
);

export const BasesLegales = ({ toggled, onClick, onClose }) => (
  <Column>
    {renderCard(onClick)}
    {renderModal(toggled, onClick, onClose)}

  </Column>
);

BasesLegales.propTypes = {
  toggled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export const ModalExample = compose(
  withToggle,
  withHandlers({
    onClick: ({ toggle }) => event => {
      event.preventDefault();
      toggle();
    },
    onClose: ({ toggle }) => () => toggle()
  })
)(BasesLegales);

export default ModalExample;