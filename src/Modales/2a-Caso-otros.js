import React from "react";
import PropTypes from "prop-types";
import { compose, withHandlers } from "recompose";
import '@scotia/canvas-dom/css/index.css';
import {
  Modal,
  Direction,
  withToggle,
  Column,
  Margin,
  ButtonPill,
  TextSmall,
  ButtonContinue,
} from "@scotia/canvas-react";

const renderCard = onClick => (
    
    <ButtonContinue onClick={onClick}>Consultar</ButtonContinue>
              
);

const renderModal = (toggled, onClick, onClose) => (
  <Modal isOpen={toggled} onClose={onClose} showButtonClose>

    <Margin
    lg={24}
    xs={24}
    side="bottom">
    <TextSmall className='bold'>
    ¿Quieres enviar la información? 
    </TextSmall>
    </Margin>

    <Margin
    lg={24}
    xs={24}
    side="bottom">
    <TextSmall>
    La actualización se realizará en dos semanas. 
    </TextSmall>
    </Margin>

    <Margin
    lg={36}
    xs={36}
    side="bottom">
    <Direction className='f-derecha'>
    <ButtonPill className='m-20 bla'>
        Cancelar
    </ButtonPill>
    <ButtonPill className='m-20'>
        Enviar
    </ButtonPill>
    </Direction>
    </Margin>
    
  </Modal>
);

export const CasoOtros = ({ toggled, onClick, onClose }) => (
  <Column>
    {renderCard(onClick)}
    {renderModal(toggled, onClick, onClose)}

  </Column>
);

CasoOtros.propTypes = {
  toggled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export const ModalExample = compose(
  withToggle,
  withHandlers({
    onClick: ({ toggle }) => event => {
      event.preventDefault();
      toggle();
    },
    onClose: ({ toggle }) => () => toggle()
  })
)(CasoOtros);

export default ModalExample;