import React, { Component } from 'react';
import '@scotia/canvas-dom/css/index.css';
import zenit from './assets/zenit-seguros-logo.png';
import liberty from './assets/liberty-seguros-logo.png';
import bnp from './assets/bnp-paribas-logo.png';
import  EnviarDetalles  from './Modales/enviar-detalles';
import  TablaComparativa  from './Modales/Tabla-comparativa';
import  TablaComparativaMobile  from './Modales/Tabla-comparativa-mobile';
import infoShield from './assets/info-shield.svg';
import {Heading,
  SubHeading,
  Ruler,
  Grid,
  Direction,
  Column,
  Margin,
  IconCalendar,
  Text,
  Card,
  TextSmall,
  TextCaption,
  Badge,
  IconDollar,
  ButtonPrimary,
  BuddyTipText,
  BuddyTipFlat,
  BuddyTipIcon,
ButtonPill
} from '@scotia/canvas-react';


class App extends Component {
  render() {
    return (

      <Grid>
          <Column lgOffset={2}
            lg={8}
            xs={18}>
            <Margin
              lg={36}
              xs={36}
              side="top">
              <div className="hide--xs">
                <SubHeading>
                  Seguro Automotriz
                </SubHeading>
              </div>
              <Heading>
                Elije el plan a tu medida
              </Heading>
              <Margin lg={12} side="top"></Margin>
              <Ruler className="hide--xs"/>
            </Margin>
          </Column>

          <Column lgOffset={3}
            lg={6}
            xs={12}>
            <Margin
              lg={24}
              xs={24}>
              <BuddyTipFlat>
              <BuddyTipIcon>
            <img src={infoShield} alt="icon"/>
            </BuddyTipIcon>
            <BuddyTipText>
            Si tienes dudas o consultas puedes comunicarte al 600 670 0500
            </BuddyTipText>
          </BuddyTipFlat>
            </Margin>
          </Column>

          <Direction>
            <Column lgOffset={3}
              lg={2}
              xs={18}>
              <Margin
                lg={12}
                xs={12}
                side="bottom">
                <Card  className='p24'>
                  <Badge variant="success" className='badge-card displace-card'>
                    Mejor precio
                  </Badge>
                  <Margin
                    lg={18}
                    xs={18}>
                    <img src={zenit} alt="logo" className='card-logo' />
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <TextCaption>
                      12 cuotas de
                    </TextCaption>
                    <Direction>
                      <Text>
                        $24.500
                      </Text>
                      <TextCaption>
                        (UF 5)
                      </TextCaption>
                    </Direction>
                    <TextCaption className='card-caption-text'>
                      deducible UF 10
                    </TextCaption>
                  </Margin>
                  <TextCaption>
                    Plan Premium
                  </TextCaption>
                  <TextCaption>
                    Vigencia Anual
                  </TextCaption>
                  <Ruler />
                  <TextCaption className='card-caption-text'>
                    GifCard dto 25%
                  </TextCaption>
                  <Margin
                  lg={18}
                  xs={18}
                  side="top">
                  <EnviarDetalles/>
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="top">
                    <ButtonPill className='obtener-plan'>Obtener plan</ButtonPill> 
                  </Margin>
                </Card>
              </Margin>
            </Column>

            <Column
              lg={2}
              xs={18}>
              <Margin
                lg={12}
                xs={12}
                side="bottom">
                <Card  className='p24'>
                  <Badge variant="success" className='badge-card displace-card'>
                    Mejor precio
                  </Badge>
                  <Margin
                    lg={18}
                    xs={18}>
                    <img src={liberty} alt="logo" className='card-logo' />
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <TextCaption>
                      12 cuotas de
                    </TextCaption>
                    <Direction>
                      <Text>
                        $24.500
                      </Text>
                      <TextCaption>
                        (UF 5)
                      </TextCaption>
                    </Direction>
                    <TextCaption className='card-caption-text'>
                      deducible UF 10
                    </TextCaption>
                  </Margin>
                  <TextCaption>
                    Plan Premium
                  </TextCaption>
                  <TextCaption>
                    Vigencia Anual
                  </TextCaption>
                  <Ruler />
                  <TextCaption className='card-caption-text'>
                    GifCard dto 25%
                  </TextCaption>
                  <Margin
                  lg={18}
                  xs={18}
                  side="top">
                  <EnviarDetalles/>
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="top">
                    <ButtonPill className='obtener-plan'>Obtener plan</ButtonPill> 
                  </Margin>
                </Card>
              </Margin>
            </Column>

            <Column
              lg={2}
              xs={18}>
              <Margin
                lg={12}
                xs={12}
                side="bottom">
                <Card  className='p24'>
                  <Badge variant="success" className='badge-card displace-card'>
                    Mejor precio
                  </Badge>
                  <Margin
                    lg={18}
                    xs={18}>
                    <img src={bnp} alt="logo" className='card-logo' />
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <TextCaption>
                      12 cuotas de
                    </TextCaption>
                    <Direction>
                      <Text>
                        $24.500
                      </Text>
                      <TextCaption>
                        (UF 5)
                      </TextCaption>
                    </Direction>
                    <TextCaption className='card-caption-text'>
                      deducible UF 10
                     </TextCaption>
                  </Margin>
                  <TextCaption>
                    Plan Premium
                  </TextCaption>
                  <TextCaption>
                    Vigencia Anual
                  </TextCaption>
                  <Ruler />
                  <TextCaption className='card-caption-text'>
                    GifCard dto 25%
                  </TextCaption>
                  <Margin
                  lg={18}
                  xs={18}
                  side="top">
                  <EnviarDetalles/>
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="top">
                    <ButtonPill className='obtener-plan'>Obtener plan</ButtonPill> 
                  </Margin>
                </Card>
              </Margin>
            </Column>
          </Direction>

        <Column lgOffset={2}
        lg={8}
        xs={12}>
            <Margin
            lg={24}
            xs={24}
            side="bottom" className="hide--xs">
            <TablaComparativa/>
            </Margin>
            <Margin
            lg={24}
            xs={24}
            side="bottom" className="hide--lg">
            <TablaComparativaMobile />
            </Margin>
          </Column>

          <Column lgOffset={2}
            lg={8}
            xs={18}>
            <Margin
              lg={24}
              xs={24}
              side="bottom">
              <TextSmall>
              Si deseas configurar tu propio plan, selecciona el deducible, tipo de producto y vigencia.
                </TextSmall>
            </Margin>
          </Column>

          <Margin
        lg={36} xs={48}
        side="bottom">
        <Direction>
            <Column lgOffset={2}
            lg={3}
            xs={12}>
            <Margin lg={24} xs={24} side="bottom">
            <TextSmall>Deducible</TextSmall>
            <Direction>
            <div className="check-card2">
              <div className="no-flex centrar">
              <TextCaption className="lh16">
                UF 0
              </TextCaption>
              </div>
              </div>

              <div className="check-card2">
              <div className="no-flex centrar">
              <TextCaption className="lh16">
                UF 3
              </TextCaption>
              </div>
              </div>

                <div className="check-card2">
              <div className="no-flex centrar">
              <TextCaption className="lh16">
                UF 5
              </TextCaption>
              </div>
              </div>

                <div className="check-card2">
              <div className="no-flex centrar">
              <TextCaption className="lh16">
                UF 10
              </TextCaption>
              </div>
              </div>
            </Direction>
            </Margin>
            </Column>

            
            <Column lg={3}
            xs={12}>
            <Margin xs={24} side="bottom">
            <Direction className="space-between">
            <Column lg={6} className="p0">
            <TextSmall>Producto</TextSmall>
            <Direction>
            <div className="check-card2">
              <div className="no-flex centrar">
              <IconDollar className="mt5"/>
              <TextCaption className="lh16 ver-detalles">
                Básico
              </TextCaption>
              </div>
              </div>

            <div className="check-card2">
              <div className="no-flex centrar">
              <IconDollar className="mt5"/>
              <TextCaption className="lh16 ver-detalles">
                Premium
              </TextCaption>
              </div>
              </div>

            </Direction>
            </Column>
            <Column lg={6} className="p0">
            <TextSmall>Vigencia</TextSmall>
            <Direction>
            <div className="check-card2">
              <div className="no-flex centrar">
              <IconCalendar className="mt5"/>
              <TextCaption className="lh16 ver-detalles">
                Anual
              </TextCaption>
              </div>
              </div>

            <div className="check-card2">
              <div className="no-flex centrar">
              <IconCalendar className="mt5"/>
              <TextCaption className="lh16 ver-detalles">
                Bienal
              </TextCaption>
              </div>
              </div>
            </Direction>
            </Column>
            </Direction>
            </Margin>
            </Column>
            
            <Column lg={2} xs={12} className="flex-end-right">
            <ButtonPrimary className="h56">Buscar</ButtonPrimary>
            </Column>
            
        </Direction>
        </Margin>

          <Direction className="f-centrar">
            <Column
            lgOffset={2}
            xsOffset={2}
              lg={2}
              xs={8}>
              <Margin
                lg={12}
                xs={72}
                side="bottom">
                <Card  className='p24'>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <img src={zenit} alt="logo" className='card-logo' />
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <TextCaption>
                      12 cuotas de
                    </TextCaption>
                    <Direction>
                      <Text>
                        $20.200
                      </Text>
                      <TextCaption>
                        (UF 1)
                      </TextCaption>
                    </Direction>
                    <TextCaption className='card-caption-text'>
                      deducible UF 10
                    </TextCaption>
                  </Margin>
                  <Ruler />
                  <TextCaption className='card-caption-text'>
                    GifCard dto 25%
                  </TextCaption>
                  <Margin
                  lg={18}
                  xs={18}
                  side="top">
                  <EnviarDetalles/>
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="top">
                    <ButtonPill className='obtener-plan'>Obtener plan</ButtonPill> 
                  </Margin>
                </Card>
              </Margin>
            </Column>

            <Column
            lgOffset={0}
            xsOffset={2}
              lg={2}
              xs={8}>
              <Margin
                lg={12}
                xs={72}
                side="bottom">
                <Card  className='p24'>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <img src={liberty} alt="logo" className='card-logo' />
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <TextCaption>
                      12 cuotas de
                    </TextCaption>
                    <Direction>
                      <Text>
                        $23.500
                      </Text>
                      <TextCaption>
                        (UF 1,5)
                      </TextCaption>
                    </Direction>
                    <TextCaption className='card-caption-text'>
                      deducible UF 3
                    </TextCaption>
                  </Margin>
                  <Ruler />
                  <TextCaption className='card-caption-text'>
                    GifCard dto 25%
                  </TextCaption>
                  <Margin
                  lg={18}
                  xs={18}
                  side="top">
                  <EnviarDetalles/>
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="top">
                    <ButtonPill className='obtener-plan'>Obtener plan</ButtonPill> 
                  </Margin>
                </Card>
              </Margin>
            </Column>

            <Column
            xsOffset={2}
              lg={2}
              xs={8}>
              <Margin
                lg={12}
                xs={72}
                side="bottom">
                <Card  className='p24'>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <img src={bnp} alt="logo" className='card-logo' />
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <TextCaption>
                      12 cuotas de
                    </TextCaption>
                    <Direction>
                      <Text>
                        $24.000
                      </Text>
                      <TextCaption>
                        (UF 2)
                      </TextCaption>
                    </Direction>
                    <TextCaption className='card-caption-text'>
                      deducible UF 10
                     </TextCaption>
                  </Margin>
                  <Ruler />
                  <TextCaption className='card-caption-text'>
                    GifCard dto 25%
                  </TextCaption>
                  <Margin
                  lg={18}
                  xs={18}
                  side="top">
                  <EnviarDetalles/>
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="top">
                    <ButtonPill className='obtener-plan'>Obtener plan</ButtonPill> 
                  </Margin>
                </Card>
              </Margin>
            </Column>

            <Column
            xsOffset={2}
              lg={2}
              xs={8}>
              <Margin
                lg={12}
                xs={72}
                side="bottom">
                <Card  className='p24'>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <img src={zenit} alt="logo" className='card-logo' />
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <TextCaption>
                      12 cuotas de
                    </TextCaption>
                    <Direction>
                      <Text>
                        $26.000
                      </Text>
                      <TextCaption>
                        (UF 2,5)
                      </TextCaption>
                    </Direction>
                    <TextCaption className='card-caption-text'>
                      deducible UF 3
                    </TextCaption>
                  </Margin>
                  <Ruler />
                  <TextCaption className='card-caption-text'>
                    GifCard dto 25%
                  </TextCaption>
                  <Margin
                  lg={18}
                  xs={18}
                  side="top">
                  <EnviarDetalles/>
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="top">
                    <ButtonPill className='obtener-plan'>Obtener plan</ButtonPill> 
                  </Margin>
                </Card>
              </Margin>
            </Column>
          </Direction>

          <Direction className="f-centrar">
            <Column
            lgOffset={2}
            xsOffset={2}
              lg={2}
              xs={8}>
              <Margin
                lg={12}
                xs={72}
                side="bottom">
                <Card  className='p24'>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <img src={liberty} alt="logo" className='card-logo' />
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <TextCaption>
                      12 cuotas de
                    </TextCaption>
                    <Direction>
                      <Text>
                        $25.500
                      </Text>
                      <TextCaption>
                        (UF 3)
                      </TextCaption>
                    </Direction>
                    <TextCaption className='card-caption-text'>
                      deducible UF 10
                    </TextCaption>
                  </Margin>
                  <Ruler />
                  <TextCaption className='card-caption-text'>
                    GifCard dto 40%
                  </TextCaption>
                  <Margin
                  lg={18}
                  xs={18}
                  side="top">
                  <EnviarDetalles/>
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="top">
                    <ButtonPill className='obtener-plan'>Obtener plan</ButtonPill> 
                  </Margin>
                </Card>
              </Margin>
            </Column>

            <Column
            xsOffset={2}
              lg={2}
              xs={8}>
              <Margin
                lg={12}
                xs={72}
                side="bottom">
                <Card  className='p24'>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <img src={bnp} alt="logo" className='card-logo' />
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <TextCaption>
                      12 cuotas de
                    </TextCaption>
                    <Direction>
                      <Text>
                        $25.700
                      </Text>
                      <TextCaption>
                        (UF 3,5)
                      </TextCaption>
                    </Direction>
                    <TextCaption className='card-caption-text'>
                      deducible UF 3
                    </TextCaption>
                  </Margin>
                  <Ruler />
                  <TextCaption className='card-caption-text'>
                  dcto. TC Scotiabank
                  </TextCaption>
                  <Margin
                  lg={18}
                  xs={18}
                  side="top">
                  <EnviarDetalles/>
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="top">
                    <ButtonPill className='obtener-plan'>Obtener plan</ButtonPill> 
                  </Margin>
                </Card>
              </Margin>
            </Column>

            <Column
            xsOffset={2}
              lg={2}
              xs={8}>
              <Margin
                lg={12}
                xs={72}
                side="bottom">
                <Card  className='p24'>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <img src={zenit} alt="logo" className='card-logo' />
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <TextCaption>
                      12 cuotas de
                    </TextCaption>
                    <Direction>
                      <Text>
                        $26.000
                      </Text>
                      <TextCaption>
                        (UF 4)
                      </TextCaption>
                    </Direction>
                    <TextCaption className='card-caption-text'>
                      deducible UF 3
                     </TextCaption>
                  </Margin>
                  <Ruler />
                  <TextCaption className='card-caption-text'>
                    GifCard dto 5%
                  </TextCaption>
                  <Margin
                  lg={18}
                  xs={18}
                  side="top">
                  <EnviarDetalles/>
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="top">
                    <ButtonPill className='obtener-plan'>Obtener plan</ButtonPill> 
                  </Margin>
                </Card>
              </Margin>
            </Column>

            <Column
            xsOffset={2}
              lg={2}
              xs={8}>
              <Margin
                lg={12}
                xs={72}
                side="bottom">
                <Card  className='p24'>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <img src={liberty} alt="logo" className='card-logo' />
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="bottom">
                    <TextCaption>
                      12 cuotas de
                    </TextCaption>
                    <Direction>
                      <Text>
                        $30.000
                      </Text>
                      <TextCaption>
                        (UF 8)
                      </TextCaption>
                    </Direction>
                    <TextCaption className='card-caption-text'>
                      deducible UF 10
                    </TextCaption>
                  </Margin>
                  <Ruler />
                  <TextCaption className='card-caption-text'>
                    GifCard dto 25%
                  </TextCaption>
                  <Margin
                  lg={18}
                  xs={18}
                  side="top">
                  <EnviarDetalles/>
                  </Margin>
                  <Margin
                    lg={18}
                    xs={18}
                    side="top">
                    <ButtonPill className='obtener-plan'>Obtener plan</ButtonPill> 
                  </Margin>
                </Card>
              </Margin>
            </Column>
          </Direction>

      
          <Column lgOffset={5}
        lg={4}
        xs={12}>
            <Margin
            lg={24}
            xs={24}
            side="bottom" className="xs-centrar">
            <ButtonPill className='ver-tabla-comparativa'>Cargar más resultados</ButtonPill>
            </Margin>
          </Column>
          

      </Grid>
    );

  }
}



export default App;