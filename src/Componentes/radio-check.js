import '@scotia/canvas-dom/css/index.css';



var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _label = require('../../node_modules/@scotia/canvas-react/lib/core/components/form/label');

var _input = require('../../node_modules/@scotia/canvas-react/lib/core/components/form/input');

var _iconToggleCircle = require('../../node_modules/@scotia/canvas-react/lib/core/components/svg-icons/icon-toggle-circle');

var _srOnly = require('../../node_modules/@scotia/canvas-react/lib/core/components/support/sr-only');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var getClassNames = function getClassNames(_ref) {
  var error = _ref.error;
  return (0, _classnames2.default)('form__input', 'form__input--inline', 'form__input--radio', { 'form__input--error': error });
};

var InputRadio = function InputRadio(_ref2) {
  var id = _ref2.id,
      label = _ref2.label,
      disabled = _ref2.disabled,
      error = _ref2.error,
      name = _ref2.name,
      describedBy = _ref2.describedBy,
      props = _objectWithoutProperties(_ref2, ['id', 'label', 'disabled', 'error', 'name', 'describedBy']);

  return _react2.default.createElement(
    'label',
    { htmlFor: id, className: getClassNames({ error: error }) },
    _react2.default.createElement(_input.Input, _extends({}, props, {
      name: name,
      type: 'radio',
      className: 'input--radio',
      id: id,
      disabled: disabled,
      error: error,
      'aria-describedby': name + '-error ' + describedBy.join(' '),
      'aria-invalid': error
    })),
    _react2.default.createElement(
      'span',
      { className: 'radio' },
      _react2.default.createElement('span', { className: 'radio__circle--big' }),
      _react2.default.createElement(_iconToggleCircle.IconToggleCircle, { className: 'radio__circle--small' })
    ),
    _react2.default.createElement(
      _label.Label,
      {
        component: 'div',
        className: 'label--inline label--radio'
      },
      error ? _react2.default.createElement(
        _srOnly.SROnly,
        null,
        'Error'
      ) : null,
      label
    )
  );
};

export const RadioCheck =
InputRadio.propTypes = {
  name: _propTypes2.default.string.isRequired,
  id: _propTypes2.default.string.isRequired,
  label: _propTypes2.default.node.isRequired,
  disabled: _propTypes2.default.bool,
  error: _propTypes2.default.bool,
  describedBy: _propTypes2.default.arrayOf(_propTypes2.default.string)
};

InputRadio.defaultProps = {
  disabled: false,
  error: false,
  describedBy: []
};
InputRadio.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'InputRadio',
  'props': {
    'disabled': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'error': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'describedBy': {
      'defaultValue': {
        'value': '[]',
        'computed': false
      },
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'name': {
      'type': {
        'name': 'string'
      },
      'required': true,
      'description': ''
    },
    'id': {
      'type': {
        'name': 'string'
      },
      'required': true,
      'description': ''
    },
    'label': {
      'type': {
        'name': 'node'
      },
      'required': true,
      'description': ''
    }
  }
};

export default RadioCheck;