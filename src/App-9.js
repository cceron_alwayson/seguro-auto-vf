import React, { Component } from 'react';
import '@scotia/canvas-dom/css/index.css';
import Sello from './assets/sello.svg';
import {
  Heading,
  SubHeading,
  Ruler,
  Grid,
  Direction,
  Column,
  Margin,
  Card,
  IconSuccessIllustrative,
  IconDownload,
  Text,
  BrandLine,
  TextSmall,
  IconMail,
  ButtonPrimary
} from '@scotia/canvas-react';

class App extends Component {
  render() {
    return (

      <Grid>
        <Direction>
          <Column lgOffset={2}
            lg={8}
            xs={12}>
            <Margin
              lg={36}
              xs={36}>
              <div className="hide--xs">
                <SubHeading>
                  Seguro Automotriz
                </SubHeading>
              </div>
              <Heading>
                Tu comprobante
              </Heading>
              <Ruler className="hide--xs" />
            </Margin>
          </Column>

          <Column lgOffset={2}
            lg={8}
            xs={12} className='p72-comprobante'>
            <Margin
              lg={36}
              xs={36}
              side="bottom">

              <Card className="card-comprobante">

                <Margin
                  lg={36}
                  xs={18}
                  side="bottom">

                  <Direction className="invertir">

                    <Column
                      lg={4} xs={12} lgOffset={4} className='centrar'>
                      <IconSuccessIllustrative size={72} />
                    </Column>

                    <Column
                      lg={4} xs={12} className='p0'>
                        <Margin xs={24} side="bottom">
                      <Direction className='flex-vc-fe'>
                        <TextSmall className='pr10'>
                          24 Febrero 2019
                        </TextSmall>
                        <div className='circulo-icon'>
                          <IconDownload className='download-icon' />
                        </div>
                      </Direction>
                      </Margin>
                    </Column>

                  </Direction>

                </Margin>

                <Margin
                  lg={24}
                  xs={24}
                  side="bottom" className='centrar'>
                  <SubHeading>
                    Propuesta de Seguro Automotriz
                </SubHeading>
                </Margin>

                <Text className='izquierda'>Hemos procesado tu solicitud y tu propuesta de Seguro ha sido generada con el Nro.:  <bold className="bold">1234 123456 88</bold>
                </Text>

                <Margin
                  lg={24}
                  xs={24}
                  className='izquierda'>
                  <BrandLine />
                </Margin>

                <Margin
                  lg={24}
                  xs={24}
                  side="bottom">
                  <SubHeading className='izquierda'>
                    Antecedentes del Seguro
                </SubHeading>
                </Margin>

                <Margin
                  lg={36}
                  xs={36}
                  side="bottom">
                  <Direction>
                    <Column
                      lg={6} xs={12} className='izquierda p0 b-right'>

                      <Margin
                        lg={24}
                        xs={24}
                        side="bottom">
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <TextSmall>
                            Aseguradora
                    </TextSmall>
                        </Margin>
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <SubHeading className="text--light">
                            Zenit
                    </SubHeading>
                        </Margin>
                      </Margin>

                      <Margin
                        lg={24}
                        xs={24}
                        side="bottom">
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <TextSmall>
                            Corredor de Seguro
                    </TextSmall>
                        </Margin>
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <SubHeading className="text--light">
                            Jhon Dude
                    </SubHeading>
                        </Margin>
                      </Margin>

                      <Margin
                        lg={24}
                        xs={24}
                        side="bottom">
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <TextSmall>
                            Plan
                    </TextSmall>
                        </Margin>
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <SubHeading className="text--light">
                            Básico
                    </SubHeading>
                        </Margin>
                      </Margin>

                      <Margin
                        lg={12}
                        xs={12}
                        side="bottom">
                        <TextSmall>
                          Valor mensual
                    </TextSmall>
                      </Margin>
                      <Margin xs={24} side="bottom">
                      <SubHeading className="text--light">
                        $26.000 (UF 10)
                    </SubHeading>
                    </Margin>
                    </Column>

                    <Column
                      lg={6} xs={12} className='izquierda pl24'>
                      <div className="no-flex">
                        <Margin
                          lg={24}
                          xs={24}
                          side="bottom">
                          <Margin
                            lg={12}
                            xs={12}
                            side="bottom">
                            <TextSmall>
                              RUT de Corredor
                    </TextSmall>
                          </Margin>
                          <Margin
                            lg={12}
                            xs={12}
                            side="bottom">
                            <SubHeading className="text--light">
                              1234567-8
                    </SubHeading>
                          </Margin>
                        </Margin>

                        <Margin
                          lg={24}
                          xs={24}
                          side="bottom">
                          <Margin
                            lg={12}
                            xs={12}
                            side="bottom">
                            <TextSmall>
                              Vigencia
                    </TextSmall>
                          </Margin>
                          <Margin
                            lg={12}
                            xs={12}
                            side="bottom">
                            <SubHeading className="text--light">
                              Anual
                    </SubHeading>
                          </Margin>
                        </Margin>

                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <TextSmall>
                            Deducible
                    </TextSmall>
                        </Margin>
                        <SubHeading className="text--light">
                          UF 10
                    </SubHeading>
                      </div>
                    </Column>
                  </Direction>
                </Margin>

                <Margin
                  lg={36}
                  xs={24}
                  side="bottom" className='izquierda'>
                  <BrandLine />
                </Margin>

                <Margin
                  lg={24}
                  xs={24}
                  side="bottom">
                  <SubHeading className='izquierda'>
                    Antecedentes del asegurado
                </SubHeading>
                </Margin>

                <Margin
                  lg={36}
                  xs={24}
                  side="bottom">
                  <Direction>
                    <Column
                      lg={6} xs={12} className='izquierda p0 b-right'>
                      <Margin
                        lg={36}
                        xs={24}
                        side="bottom">
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <TextSmall>
                            Nombre
                    </TextSmall>
                        </Margin>
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <SubHeading className="text--light">
                            Hugo
                    </SubHeading>
                        </Margin>
                      </Margin>

                      <Margin
                        lg={36}
                        xs={24}
                        side="bottom">
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <TextSmall>
                            Apellido
                    </TextSmall>
                        </Margin>
                        <Margin
                          lg={12}
                          xs={24}
                          side="bottom">
                          <SubHeading className="text--light">
                            Saavedra
                    </SubHeading>
                        </Margin>
                      </Margin>

                      <Margin
                        lg={12}
                        xs={12}
                        side="bottom">
                        <TextSmall>
                          Apellido Materno
                    </TextSmall>
                      </Margin>
                      <Margin xs={24} side="bottom">
                      <SubHeading className="text--light">
                        Cortez
                    </SubHeading>
                    </Margin>
                    </Column>

                    <Column
                      lg={6} xs={12} className='izquierda pl24'>
                      <Margin
                        lg={36}
                        xs={24}
                        side="bottom">
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <TextSmall>
                            Rut
                    </TextSmall>
                        </Margin>
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <SubHeading className="text--light">
                            24954522-9
                    </SubHeading>
                        </Margin>
                      </Margin>

                      <Margin
                        lg={12}
                        xs={12}
                        side="bottom">
                        <TextSmall>
                          Teléfono
                    </TextSmall>
                      </Margin>
                      <SubHeading className="text--light">
                        56 **** 7839
                    </SubHeading>
                    </Column>
                  </Direction>
                </Margin>

                <Margin
                  lg={36}
                  xs={24} side="bottom" className="izquierda">
                  <BrandLine />
                </Margin>

                <Margin
                  lg={24}
                  xs={24}
                  side="bottom">
                  <SubHeading className='izquierda'>
                    Antecedentes del vehículo
                </SubHeading>
                </Margin>

                <Margin
                  lg={36}
                  xs={12}
                  side="bottom">
                  <Direction>
                    <Column
                      lg={6} xs={12} className='izquierda p0 b-right'>
                      <Margin
                        lg={24}
                        xs={24}
                        side="bottom">
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <TextSmall>
                            Marca
                    </TextSmall>
                        </Margin>
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <SubHeading className="text--light">
                            Toyota
                    </SubHeading>
                        </Margin>
                      </Margin>

                      <Margin
                        lg={24}
                        xs={24}
                        side="bottom">
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <TextSmall>
                            Año
                    </TextSmall>
                        </Margin>
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <SubHeading className="text--light">
                            2015
                    </SubHeading>
                        </Margin>
                      </Margin>

                    </Column>

                    <Column
                      lg={6} xs={12} className='izquierda pl24'>
                      <Margin
                        lg={24}
                        xs={24}
                        side="bottom">
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <TextSmall>
                            Modelo
                    </TextSmall>
                        </Margin>
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <SubHeading className="text--light">
                            AS345
                    </SubHeading>
                        </Margin>
                      </Margin>

                      <Margin
                        lg={24}
                        xs={12}
                        side="bottom">
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <TextSmall>
                            Patente
                    </TextSmall>
                        </Margin>
                        <Margin
                          lg={12}
                          xs={12}
                          side="bottom">
                          <SubHeading className="text--light">
                            BZ AA 55
                    </SubHeading>
                        </Margin>
                      </Margin>

                    </Column>
                  </Direction>
                </Margin>

                <Margin
                  lg={36} xs={24} side="bottom" className="izquierda">
                  <BrandLine />
                </Margin>

                <Margin
                  lg={24}
                  xs={24}
                  side="bottom">
                  <SubHeading className='izquierda'>
                    Plan de pago elegido
                </SubHeading>
                </Margin>

                <Direction>
                  <Column
                    lg={6} xs={12} className='izquierda p0 b-right'>
                    <Margin
                      lg={24}
                      xs={24}
                      side="bottom">
                      <Margin
                        lg={12}
                        xs={12}
                        side="bottom">
                        <TextSmall>
                          Nombre del titular
                    </TextSmall>
                      </Margin>
                      <Margin
                        lg={12}
                        xs={12}
                        side="bottom">
                        <SubHeading className="text--light">
                          Hugo
                    </SubHeading>
                      </Margin>
                    </Margin>

                    <Margin
                      lg={24}
                      xs={24}
                      side="bottom">
                      <Margin
                        lg={12}
                        xs={12}
                        side="bottom">
                        <TextSmall>
                          Apellido Paterno
                    </TextSmall>
                      </Margin>
                      <Margin
                        lg={12}
                        xs={12}
                        side="bottom">
                        <SubHeading className="text--light">
                          Saavedra
                    </SubHeading>
                      </Margin>
                    </Margin>

                  </Column>

                  <Column
                    lg={6} xs={12} className='izquierda pl24'>
                    <Margin
                      lg={24}
                      xs={24}
                      side="bottom">
                      <Margin
                        lg={12}
                        xs={12}
                        side="bottom">
                        <TextSmall>
                        Forma de pago
                    </TextSmall>
                      </Margin>
                      <Margin
                        lg={12}
                        xs={12}
                        side="bottom">
                        <SubHeading className="text--light">
                        PAC
                    </SubHeading>
                      </Margin>
                    </Margin>

                    <Margin
                      lg={24}
                      xs={24}
                      side="bottom">
                      <Margin
                        lg={12}
                        xs={12}
                        side="bottom">
                        <TextSmall>
                          Nro. Cuenta Corriente
                    </TextSmall>
                      </Margin>
                      <SubHeading className="text--light">
                        **** 7890
                    </SubHeading>
                    </Margin>
                    <div className="f-right-center">
                      <img src={Sello} className="w40" />
                    </div>
                  </Column>
                </Direction>

                <Margin
                  lg={24}
                  xs={24}>
                  <Ruler />
                </Margin>

                <Direction className='f-centrar'>
                  <IconMail className='pr10' />
                  <TextSmall>
                    Enviamos el comprobante a tu correo registrado a·····@g···.com
                </TextSmall>
                </Direction>

              </Card>
            </Margin>

          </Column>

          <Column
            lgOffset={3}
            lg={6}
            xs={12}>
            <Margin
              lg={36}
              xs={36}
              side="bottom">
              <Direction className="f-centrar">
                  <ButtonPrimary>Ir a tu Cuenta</ButtonPrimary>
              </Direction>
            </Margin>
          </Column>

        </Direction>
      </Grid>
    );

  }
}

export default App;