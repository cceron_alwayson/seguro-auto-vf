import React, { Component } from 'react';
import '@scotia/canvas-dom/css/index.css';
import home from './assets/home.svg';
import build from './assets/build.svg';
import {
  InputSelect,
  InputCurrency,
  InputText,
  Direction,
  Grid,
  Margin,
  SubHeading,
  Heading,
  Ruler,
  Column,
  TextSmall,
  ButtonBack,
  ButtonContinue,
  TextCaption,
} from '@scotia/canvas-react';


class App extends Component {
  render() {
    return (

      <Grid>

        <Column lgOffset={2}
          lg={8}
          xs={18}>
          <Margin
            lg={36}
            xs={36}
            side="top">
            <div className="hide--xs">
              <SubHeading>
                Seguro Automotriz
                </SubHeading>
            </div>
            <Heading>
              Datos de asegurado
              </Heading>
            <Ruler className="hide--xs" />
          </Margin>
        </Column>

        <Margin lg={24}
          xs={24}>
          <Column lgOffset={2}
            lg={8}
            xs={18}>
            <Margin
              lg={24}
              xs={24}
              side="bottom">
              <TextSmall>
                Completa los datos del dueño del vehículo
                </TextSmall>
            </Margin>
          </Column>

          <Margin lg={36} side="top">
           
              <Column lgOffset={2} lg={3} xs={12} className="xs-p0">

                <Margin lg={18} side="bottom">
                  <TextSmall>
                    Nombre
                  </TextSmall>
                </Margin>
                <Margin lg={24} xs={24} side="bottom">
                  <SubHeading>
                    Hugo
                  </SubHeading>
                </Margin>
                </Column>

              <Direction>

              <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
                <Margin lg={18} xs={18} side="bottom">
                  <TextSmall>
                    Apellido Paterno
                  </TextSmall>
                </Margin>

                <Margin lg={24} xs={24} side="bottom">
                  <SubHeading>
                    Saavedra
                  </SubHeading>
                </Margin>
                </Column>

                <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
                <Margin lg={18} xs={18} side="bottom">
                    <TextSmall>
                      Apellido Materno
                    </TextSmall>
                  </Margin>
                  <Margin lg={24} xs={24} side="bottom">
                    <SubHeading>
                      Cortez
                    </SubHeading>
                  </Margin>
                  </Column>

                  </Direction>

                  <Direction>

              <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
                <Margin xs={24} side="bottom">
                <InputText
                  id="input-1"
                  type="text"
                  label="E-mail"
                  placeholder="arleak@gmail.com"
                  errorMsg="Field specific error message" />
                  </Margin>
                </Column>

                <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
                  <Margin xs={36} side="bottom">
                  <InputText
                    id="input-1"
                    type="text"
                    label="Teléfono"
                    placeholder="+56 9 4238 4260"
                    errorMsg="Field specific error message" />
                    </Margin>            
                </Column>

                </Direction>
              
              <Column lg={8} xs={12} lgOffset={2} className="hide--xs">
                <Margin lg={24} side="bottom">
                  <Ruler />
                </Margin>
              </Column>
          
          </Margin>

          <Margin lg={36} xs={24} side="bottom">
            <Direction>

              <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
                <Margin xs={24} side="bottom">
                <InputSelect
                  id="select-1"
                  name="select-1"
                  label="Región"
                  defaultValue=""
                  className="test">

                  <option value="" disabled hidden>
                    texto
              </option>
                  <option value="option1">Option 1</option>
                  <option value="option2">Option 2</option>
                  <option value="option3">Option 3</option>
                </InputSelect>
                </Margin>
              </Column>
              
              <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
                <Margin xs={24} side="bottom">
              <InputSelect
                  id="select-1"
                  name="select-1"
                  label="Ciudad"
                  defaultValue=""
                  className="test">

                  <option value="" disabled hidden>
                    texto
              </option>
                  <option value="option1">Option 1</option>
                  <option value="option2">Option 2</option>
                  <option value="option3">Option 3</option>
                </InputSelect>
                </Margin>
              </Column>

              <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
              <InputSelect
                  id="select-1"
                  name="select-1"
                  label="Comuna"
                  defaultValue=""
                  className="test">

                  <option value="" disabled hidden>
                    Santiago
              </option>
                  <option value="option1">Option 1</option>
                  <option value="option2">Option 2</option>
                  <option value="option3">Option 3</option>
                </InputSelect>
              </Column>

            </Direction>
          </Margin>

          <Margin lg={36} xs={24}>
            <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
              <Margin lg={24} xs={24} side="bottom">
                <TextSmall>Tipo de vivienda</TextSmall>
                <div className="flex f-izquierda">
                  <div className="check-card">
                    <div className="no-flex centrar">
                      <img src={home} alt="icon" className="mt5" />
                      <TextCaption className="lh16">
                        Casa
                      </TextCaption>
                    </div>
                  </div>

                  <div className="check-card">
                    <div className="no-flex centrar">
                      <img src={build} alt="icon" className="mt5 icon-color" />
                      <TextCaption className="lh16">
                        Dpto.
                      </TextCaption>
                    </div>
                  </div>
                </div>
              </Margin>

            </Column>
            <Direction>
              <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
                <Margin lg={24} xs={24} side="bottom">
                  <InputText
                    id="input-1"
                    type="text"
                    label="Calle"
                    placeholder="Sevilla"
                    errorMsg="Field specific error message" />
                </Margin>
                </Column>

              <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
                <Margin lg={24} xs={24} side="bottom">
                  <InputText
                    id="input-1"
                    type="text"
                    label="Número"
                    placeholder="2323"
                    errorMsg="Field specific error message" />
                </Margin>
                </Column>

                {/* Este input se activa al chequear el radiobutton "tipo de vivienda"*/}
              <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
                <Margin xs={24}>
                <InputCurrency
                  id="input-4"
                  type="text"
                  label="Condominio/Torre"
                  placeholder="---"
                  errorMsg="Field specific error message"
                  disabled/>
                </Margin>
              </Column>

                {/* Este input se activa al chequear el radiobutton "tipo de vivienda"*/}
                <Column lgOffset={2} lg={3} xs={12} className="xs-p0">
                <InputCurrency
                  id="input-4"
                  type="text"
                  label="Departamento"
                  placeholder="---"
                  errorMsg="Field specific error message"
                  disabled/>
                  </Column>
              
            </Direction>
          </Margin>

          <Column lgOffset={2} lg={8} xs={12} className="xs-p0">
            <Margin lg={36} xs={36} side="bottom" className='space-between'>
              <ButtonBack className="r18">Volver a simular</ButtonBack>
              {/* este boton se activa tras rellenar los campos superiores */}
              <ButtonContinue className="text-red">Continuar</ButtonContinue>
            </Margin>
          </Column>
        </Margin>

      </Grid>





    );

  }
}

export default App;