import React, { Component } from 'react';
import '@scotia/canvas-dom/css/index.css';
import PopAlert from './Modales/pop-alert';
import {
  BuddyTip,
  TabAction,
  TabContent,
  InputText,
  InputCurrency,
  IconInfo,
  Direction,
  Grid,
  Margin,
  SubHeading,
  Heading,
  Ruler,
  Column,
  TextSmall,
  ButtonBack,
  ButtonContinue,
  TextCaption,
} from '@scotia/canvas-react';

class App extends Component {
  componentWillMount() {
    this.setState({ 'tabs': 1 });
  }

  Tab = (tabValue) => {
    this.setState({ 'tabs': tabValue });
  }

  render() {
    const tab = this.state.tabs;
    let tab1 = <TabAction onClick={() => this.Tab(1)} className="tab-adj2">Auto Usado</TabAction>;
    let tab2 = <TabAction onClick={() => this.Tab(2)} className="tab-adj2">Auto Nuevo</TabAction>;
    let tabContent;

    if (tab === 1) {
      tab1 = <TabAction isActive onClick={() => this.Tab(1)} className="tab-adj2">Auto Usado</TabAction>
      tabContent = <TabContent isActive>

        <Column lgOffset={2}
          lg={8}
          xs={18}>
          <Margin
            lg={24}
            xs={24}
            side="bottom">
            <TextSmall>
              Necesitamos información adicional de tu vehículo para completar tu solicitud.
              Ingresa la patente para rescatar los datos o ingrésalos para continuar.
                </TextSmall>
          </Margin>
        </Column>

        <Direction>

          <Column lgOffset={2}
            lg={4}
            xs={12} className="xs-p0">
            <Margin xs={24} side="bottom">
              <BuddyTip className="no-flex no-shadow xs-p30">
                <Margin lg={18} xs={18} side="bottom">
                  <SubHeading className="pl18">
                    Datos del vehículo
            </SubHeading>
                </Margin>

                <Direction>
                  <Column lg={6} xs={6}>
                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Marca
              </TextCaption>
                    </Margin>
                    <Margin lg={24} xs={24} side="bottom">
                      <SubHeading>
                        Toyota
              </SubHeading>
                    </Margin>

                    <Margin lg={12} xs={12} side="bottom">
                      <TextCaption>
                        Modelo
              </TextCaption>
                    </Margin>
                    <Margin lg={12} xs={12} side="bottom">
                      <SubHeading>
                        Z2309
              </SubHeading>
                    </Margin>
                  </Column>

                  <Column lg={6} xs={6} className="flex-end-bottom">
                    <div className="no-flex">
                      <Margin lg={12} side="bottom">

                        <TextCaption>
                          Año
              </TextCaption>
                      </Margin>
                      <Margin lg={12} xs={12} side="bottom">
                        <SubHeading>
                          2015
              </SubHeading>
                      </Margin>
                    </div>
                  </Column>
                </Direction>
              </BuddyTip>

            </Margin>

          </Column>

          <Column lgOffset={1}
            lg={3} xs={12} className="xs-p0">
            <InputText
              id="input-1"
              type="text"
              label="Ingresa tu patente"
              placeholder="BB BB 12"
              errorMsg="Field specific error message" />
            <Margin lg={24} xs={24} side="top">

              {/* estos input requieren cambio de estado */}
              <InputCurrency
                id="input-4"
                type="text"
                label="Número de motor"
                placeholder="Ej: AAA 123"
                errorMsg="Field specific error message"
                disabled
              />
            </Margin>
            <Margin lg={24} xs={24} side="top">
              <InputCurrency
                id="input-4"
                type="text"
                label="Número de chasis"
                placeholder="Ej: AAA 123"
                errorMsg="Field specific error message"
                disabled
              />
            </Margin>
            <Margin lg={24} xs={24} side="top">
              <InputCurrency
                id="input-4"
                type="text"
                label="Color"
                placeholder="Ej: Blanco"
                errorMsg="Field specific error message"
                disabled
              />
            </Margin>
          </Column>
        </Direction>

        <Column lgOffset={2} lg={8} xs={12} className="xs-p0">
          <Margin lg={36} xs={36} className='space-between'>
            <ButtonBack className="r18">Volver a simular</ButtonBack>
            {/* boton con cambio de estado relacionado al checkbox*/}
            <ButtonContinue>Continuar</ButtonContinue>
          </Margin>
        </Column>

      </TabContent>
    }

    if (tab === 2) {
      tab2 = <TabAction isActive onClick={() => this.Tab(2)} className="tab-adj2">Auto Nuevo</TabAction>
      tabContent = <TabContent isActive>

        <Column lgOffset={2}
          lg={8}
          xs={18}>
          <Margin
            lg={24}
            xs={24}
            side="bottom">
            <TextSmall>
              Ingresa los datos solicitados y adjunta la factura o guía de despacho, si estas superan 48 horas tu vehículo deberá ser inspeccionado. Para dudas o consultas contáctanos al 600 XXX XXX
                </TextSmall>
          </Margin>
        </Column>

        <Margin lg={36} xs={36} side="bottom">
          <Direction>
            <Column lgOffset={2}
              lg={4}
              xs={12} className="xs-p0">
              <Margin xs={24} side="bottom">
                <BuddyTip className="no-flex no-shadow xs-p30">
                  <Margin lg={18} xs={18} side="bottom">
                    <SubHeading className="pl18">
                      Datos del vehículo
            </SubHeading>
                  </Margin>

                  <Direction>
                    <Column lg={6} xs={6}>
                      <Margin lg={12} xs={12} side="bottom">
                        <TextCaption>
                          Marca
              </TextCaption>
                      </Margin>
                      <Margin lg={24} xs={24} side="bottom">
                        <SubHeading>
                          Toyota
              </SubHeading>
                      </Margin>

                      <Margin lg={12} xs={12} side="bottom">
                        <TextCaption>
                          Modelo
              </TextCaption>
                      </Margin>
                      <Margin lg={12} xs={12} side="bottom">
                        <SubHeading>
                          Z2309
              </SubHeading>
                      </Margin>
                    </Column>

                    <Column lg={6} xs={6} className="flex-end-bottom">
                      <div className="no-flex">
                        <Margin lg={12} side="bottom">

                          <TextCaption>
                            Año
              </TextCaption>
                        </Margin>
                        <Margin lg={12} xs={12} side="bottom">
                          <SubHeading>
                            2015
              </SubHeading>
                        </Margin>
                      </div>
                    </Column>
                  </Direction>
                </BuddyTip>

              </Margin>

            </Column>

            <Column lgOffset={1}
              lg={3} xs={12} className="xs-p0">
              <InputText
                id="input-1"
                type="text"
                label="Número de motor"
                placeholder="Ej: Hugo"
                errorMsg="Field specific error message" />
              <Margin lg={24} xs={24} side="top">
                <InputText
                  id="input-1"
                  type="text"
                  label="Número de chasis"
                  placeholder="Ej: Hugo"
                  errorMsg="Field specific error message" />
              </Margin>
              <Margin lg={24} xs={24} side="top">
                <InputText
                  id="input-1"
                  type="text"
                  label="Color"
                  placeholder="Ej: Blanco"
                  errorMsg="Field specific error message" />
              </Margin>
            </Column>
          </Direction>
        </Margin>

        <Column lgOffset={2}
          lg={8}>
          <Margin lg={24} xs={24} side="bottom">
            <SubHeading>
              Información sobre la factura o guía de despacho
          </SubHeading>
          </Margin>
        </Column>

        <Margin lg={24} xs={24}>


          <Direction>

            <Column lgOffset={2}
              lg={3} xs={12} className="xs-p0">
              <Margin xs={24} side="bottom">
                <div className="w100">
                  <InputText
                    id="input-1"
                    type="text"
                    label="Nro. de factura o guía de despacho"
                    placeholder="Ej: 1111111"
                    errorMsg="Field specific error message" />
                </div>
              </Margin>

              <Margin xs={24} side="bottom">
              <InputText
            id="input-1"
            type="text"
            label="Adjuntar factura o guía de despacho"
            placeholder="Adjuntar"
            errorMsg="Field specific error message" />
          <Margin lg={12} xs={12} side="top" className="flex">

            <IconInfo size={18} className="info-prop2" />
            <TextCaption className="ver-detalles">
              Formato válidos PDF, JPG, PNG. Límite 2 Mb.
            </TextCaption>
          </Margin>
          </Margin>
            </Column>

            <Column lgOffset={2}
              lg={3} xs={12} className="xs-p0">
              <div className="w100">
                {/* DataPicker */}
                <InputText
                  id="input-1"
                  type="text"
                  label="Fecha de la factura o guía de despacho"
                  placeholder="DD/MM/YYYY"
                  errorMsg="Field specific error message" />
              </div>
            </Column>

        </Direction>
        </Margin>

        <Column lgOffset={2} lg={8} xs={12} className="xs-p0">
          <Margin lg={36} xs={36} className='space-between'>
            <ButtonBack className="r18">Volver a simular</ButtonBack>
            {/* boton con cambio de estado relacionado a los input correctamente llenados */}
            {/* gatillo de modal error fecha */}
            <PopAlert />
          </Margin>
        </Column>

      </TabContent>
    }

    return (

      <Grid>

        <Column lgOffset={2}
          lg={8}
          xs={18}>
          <Margin
            lg={36}
            xs={36}
            side="top">
            <div className="hide--xs">
              <SubHeading>
                Seguro Automotriz
                </SubHeading>
            </div>
            <Heading>
              Ingresando datos del vehículo
              </Heading>
            <Ruler className="hide--xs" />
          </Margin>
        </Column>


        <Margin lg={24}
          xs={24}>
          <Direction>
            <Column lgOffset={2}
              lg={2}
              xs={6}>
              {tab1}
            </Column>
            <Column
              lg={2}
              xs={6}>
              {tab2}

            </Column>
          </Direction>


          <Column lgOffset={2}
            lg={8}>
            <Ruler className="m0" />
          </Column>
        </Margin>
        {tabContent}

      </Grid>





    );

  }
}

export default App;