import React, { Component } from 'react';
import '@scotia/canvas-dom/css/index.css';
import auto from './assets/auto.svg';
import {
  Heading, SubHeading, Ruler, Grid, Direction, Column, Margin, BrandLine, TextSmall, ButtonPrimary,
  ButtonSecondary
} from '@scotia/canvas-react';

class App extends Component {
  render() {
    return (

      <Grid>
        <Direction>
          <Column lgOffset={2}
            lg={8}
            xs={12}>
            <Margin
              lg={36}
              xs={36}>
              <div className="hide--xs">
                <SubHeading>
                  Simulador de Seguro Automotriz
                </SubHeading>
              </div>
              <Heading>
                Bienvenido
              </Heading>
              <Ruler className="hide--xs"/>
            </Margin>
          </Column>

          <Column lgOffset={4}
            lg={4}
            xs={12} className='centrar'>
            <Margin
              lg={24}
              xs={24}
              side="bottom">
            <img src={auto} alt="illustracion"/>
            </Margin>
            <Margin
              lg={24}
              xs={24}
              side="bottom">
              <Heading>
                Simula tu Seguro Automotriz
            </Heading>
            </Margin>
            <Margin
              lg={24}
              xs={24}
              side="bottom">
              <BrandLine />
            </Margin>
            <Margin
              lg={36}
              xs={36}
              side="bottom">
              <TextSmall>
                Escoge la opción que corresponda si quieres asegurar
                tu propio vehículo o deseas contratarlo para alguien más.
                </TextSmall>
            </Margin>

            <Margin
              lg={36}
              xs={36}
              side="bottom">
              <Direction className="space-between invertir">
                    <ButtonSecondary>No soy el dueño</ButtonSecondary>
                    <ButtonPrimary>Soy el dueño</ButtonPrimary>
              </Direction>
            </Margin>

            <Margin xs={36}>
            </Margin>
            
          </Column>

        </Direction>
      </Grid>
    );

  }
}

export default App;