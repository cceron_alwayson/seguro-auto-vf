import React, { Component } from 'react';
import '@scotia/canvas-dom/css/index.css';
import infoShield from './assets/info-shield.svg';
import {
  Heading,
  SubHeading,
  Ruler,
  Grid,
  Direction,
  Column,
  Margin,
  InputText,
  InputSelect,
  InputCheckbox,
  ButtonBack,
  ButtonContinue,
  IconInfo,
  TextCaption,
  BuddyTipText,
  BuddyTipFlat,
  BuddyTipIcon
} from '@scotia/canvas-react';
import CasoOtros from './Modales/caso-otros';

class App extends Component {
  render() {
    return (

      <Grid>
        <Margin lg={36} xs={24} side="bottom"></Margin>
        <Direction>
          <Column lgOffset={2}
            lg={8}
            xs={12}>
            <Margin
              lg={36}
              xs={60} side="bottom">
              <div className="hide--xs">
                <SubHeading>
                Seguro Automotriz
                </SubHeading>
              </div>
              <Margin
                lg={24}
                xs={24}
                side="bottom">
                <Heading>
                Datos del vehículo
              </Heading>
              </Margin>
              <Ruler className="hide--xs"/>
            </Margin>
          </Column>

          <Column lgOffset={2} lg={3} xs={12}>
            <Margin lg={24} xs={24} side="bottom">
              <InputSelect
                id="select-1"
                name="select-1"
                label="Marca del vehículo"
                defaultValue=""
                className="test">

                <option value="" disabled hidden>
                  Ej: Audi
              </option>
                <option value="option1">Option 1</option>
                <option value="option2">Option 2</option>
                <option value="option3">Option 3</option>
              </InputSelect>
            </Margin>
          </Column>

          <Column lgOffset={2} lg={3} xs={12}>
              <Margin lg={24} xs={24} side="bottom">
                <InputSelect
                  id="select-1"
                  name="select-1"
                  label="Modelo del vehículo"
                  defaultValue=""
                  className="test">

                  <option value="" disabled hidden>
                    Ej: AAAA230
              </option>
                  <option value="option1">Option 1</option>
                  <option value="option2">Option 2</option>
                  {/* si la opcion es "otros" se debe aplicar la clase "display-none" sobre el checkbox y la clase "display-block" sobre "ingresa tu modelo" y "enviar a correo" */}
                  <option value="option3">Otro</option>
                </InputSelect>
              </Margin>
            </Column>

          <Column lgOffset={2} lg={3} xs={12}>
            <Margin lg={36} xs={36} side="bottom">
              <InputSelect
                id="select-1"
                name="select-1"
                label="Año del vehículo"
                defaultValue=""
                className="test">

                <option value="" disabled hidden>
                  Ej: 2010
              </option>
                <option value="option1">Option 1</option>
                <option value="option2">Option 2</option>
                <option value="option3">Option 3</option>
              </InputSelect>
            </Margin>
          </Column>

          {/* ingresa tu modelo */}
          <Column lgOffset={2} lg={3} xs={12} className="display-non">
              <Margin lg={24} xs={36} side="bottom">
                <InputSelect
                  id="select-1"
                  name="select-1"
                  label="Ingresa tu modelo"
                  defaultValue=""
                  className="test">

                  <option value="" disabled hidden>
                    Ej: AAAA230
              </option>
                  <option value="option1">Option 1</option>
                  <option value="option2">Option 2</option>
                  <option value="option3">Option 3</option>
                </InputSelect>
                <Margin lg={12} xs={12} side="top" className="flex">

                  <IconInfo size={18} className="info-prop" />
                  <TextCaption className="ver-detalles">
                    Una vez enviados los documentos del vehículo, la actualización de la base de datos tardará 2 semanas.
            </TextCaption>
                </Margin>
              </Margin>
            </Column>

          <Column lgOffset={2} lg={8} xs={12} className="hide--xs">
            <Margin lg={24} xs={24} side="bottom">
              <Ruler />
            </Margin>
          </Column>

          <Column lgOffset={2} lg={12} xs={12}>
            <Margin lg={24} xs={24} side="bottom">
              <SubHeading>
                Ingresa los datos del dueño
          </SubHeading>
            </Margin>
          </Column>


          <Column lgOffset={2} lg={3} xs={12}>
            <Margin lg={24} xs={24} side="bottom">
              <InputText
                id="input-1"
                type="text"
                label="Nombre"
                placeholder="Ej: Hugo"
                errorMsg="Field specific error message" />
            </Margin>
          </Column>

          <Direction>
            <Column lgOffset={2} lg={3} xs={12}>
              <Margin lg={24} xs={24} side="bottom">
                <InputText
                  id="input-1"
                  type="text"
                  label="Apellido paterno"
                  placeholder="Ej: Saavedra"
                  errorMsg="Field specific error message" />
              </Margin>
            </Column>

            <Column lgOffset={2} lg={3} xs={12}>
              <Margin lg={24} xs={24} side="bottom">
                <InputText
                  id="input-1"
                  type="text"
                  label="Apellido materno"
                  placeholder="Ej: Cortez"
                  errorMsg="Field specific error message" />
              </Margin>
            </Column>

            <Column lgOffset={2} lg={3} xs={12}>
              <Margin lg={24} xs={24} side="bottom">
                <InputText
                  id="input-1"
                  type="text"
                  label="Rut"
                  placeholder="EJ: 12345678-9"
                  errorMsg="Field specific error message" />
              </Margin>
            </Column>

            <Column lgOffset={2} lg={3} xs={12}>
              <Margin lg={24} xs={24} side="bottom">
                <InputText
                  id="input-1"
                  type="text"
                  label="Fecha de nacimiento"
                  placeholder="dd/mm/aaaa"
                  errorMsg="Field specific error message" />
              </Margin>
            </Column>

            <Column lgOffset={2} lg={3} xs={12}>
              <Margin lg={36} xs={236} side="bottom">
                <InputText
                  id="input-1"
                  type="text"
                  label="Teléfono"
                  placeholder="56 9 **** 8937"
                  errorMsg="Field specific error message" />
              </Margin>
            </Column>

            <Column lgOffset={2} lg={3} xs={12}>
              <Margin lg={36} xs={36} side="bottom">
                <InputText
                  id="input-1"
                  type="text"
                  label="Correo"
                  placeholder="a****@gmail.com"
                  errorMsg="Field specific error message" />
              </Margin>
            </Column>

          {/* enviar a correo (cambiar el estado del input, requiere documentacion) */}
          <Column lgOffset={4} lg={4} xs={12} className="display-non xs-p0">
                <Direction>
                  <Column lg={9} xs={12}>
                    <Margin xs={24} side="bottom">
                  <InputText
                  id="input-1"
                  type="text"
                  label=""
                  placeholder="a****@gmail.com"
                  errorMsg="Field specific error message" />
                  </Margin>
                  </Column>
                  <Column lg={3} xs={12} className="flex-end-bottom">
                    <Margin xs={12} side="bottom" className="xs-w100">
                    <CasoOtros/>
                    </Margin>
                  </Column>
                </Direction>
            </Column>

          {/* checkbox */}
            <Column lgOffset={2} lg={8} xs={12} className="display-block">
              <Margin lg={24} xs={24} side="bottom">
                <InputCheckbox
                  id="dec-uso-part"
                  name="dec-uso-part"
                  label="Declaro que el vehículo es de uso particular" />
              </Margin>
            </Column>

            <Column lgOffset={4} lg={5} xs={12}>
              <Margin lg={36} xs={24} side="bottom">
              <BuddyTipFlat>
              <BuddyTipIcon>
            <img src={infoShield} alt="icon"/>
            </BuddyTipIcon>
            <BuddyTipText>
            La cobertura del Seguro Automotriz sólo está disponible para vehículos particulares.
            </BuddyTipText>
          </BuddyTipFlat>
              </Margin>
            </Column>
            </Direction>

          <Column lgOffset={2} lg={8} xs={12}>
            <Margin lg={36} xs={36} side="bottom" className='space-between'>
              <ButtonBack className="r18">Volver</ButtonBack>
              {/* boton con cambio de estado relacionado al checkbox*/}
              <ButtonContinue>Simular</ButtonContinue>
              {/* boton con cambio de estado */}
            </Margin>
          </Column>
          
          
        </Direction>
      </Grid>
    );

  }
}

export default App;