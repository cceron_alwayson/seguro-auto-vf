import React from "react";
import {TabItem, TabAction, TabContent, TabContainer} from '@scotia/canvas-react';

const tab = (props) => {
    return (
            <div className="content">
                <TabContainer>
                <TabItem>
                <TabAction  onClick={props.onClick}>
                Option one
                </TabAction>
                </TabItem>
                <TabItem>
                <TabAction isActive onClick={props.onClick}>
                Option two
                </TabAction>
                </TabItem>
                <TabItem>
                <TabAction onClick={props.onClick}>
                Option three
                </TabAction>
                </TabItem>
                <TabContent isActive>
                TabContent 1
                </TabContent>
                <TabContent>
                TabContent 2
                </TabContent>
                <TabContent>
                TabContent 3
                </TabContent>
            </TabContainer>
            </div>
    )
}

export default tab;

